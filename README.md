# amqp-demo-ruby


## Docker

`AMQP_URI=amqp://localhost:5672` - env. variable

### Producer

```
docker build -f Dockerfile-producer -t amqp-producer-demo-ruby:latest
```

### Consumer

```
docker build -f Dockerfile-consumer -t amqp-consumer-demo-ruby:latest
```
