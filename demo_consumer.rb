require 'json'
require 'bunny'

uri = ENV['AMQP_URI']
conn = Bunny.new(uri)
begin
    conn.start
rescue Bunny::TCPConnectionFailed => e
    sleep(5)
    retry
end

ch = conn.create_channel
q  = ch.queue("logs", :auto_delete => true)

puts " [*] Waiting for #{q.name}. To exit press CTRL+C"

begin
    q.subscribe(block: true) do |delivery_info, _, payload|
        puts(" -> " + payload)
    end
rescue Interrupt => _
    ch.close
    conn.close
    exit(0)
end
