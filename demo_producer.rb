require 'json'
require 'bunny'

uri = ENV['AMQP_URI']
conn = Bunny.new(uri)
begin
    conn.start
rescue Bunny::TCPConnectionFailed => e
    sleep(5)
    retry
end

ch = conn.create_channel
xch = ch.default_exchange
q  = ch.queue("logs", :auto_delete => true)

begin
    3600.times do
        msg = { message: Time.now.utc }
        data = msg.to_json
        puts data
        xch.publish(msg.to_json, routing_key: q.name)
        sleep(1)
    end
rescue Interrupt => _
    ch.close
    conn.close
    exit(0)
end
